<?php

namespace Zakharov\ComposerS3;

use Composer\Composer;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginEvents;
use Composer\Plugin\PluginInterface;
use Composer\Plugin\PreFileDownloadEvent;
use Exception;
use RuntimeException;

class S3Plugin implements PluginInterface, EventSubscriberInterface
{

    /**
     * @var array
     */
    private $config;

    /**
     * @var IOInterface
     */
    private $io;

    /**
     * @var SignatureV4
     */
    private $singer;

    public static function getSubscribedEvents()
    {
        return array(
            PluginEvents::PRE_FILE_DOWNLOAD => [
                ['onPreFileDownload', 0]
            ],
        );
    }

    public function activate(Composer $composer, IOInterface $io)
    {
        $extra        = $composer->getPackage()->getExtra();
        $this->config = isset($extra['s3'])
            ? ($extra['s3'])
            : ($composer->getConfig()->get('s3') ?: []);
        $this->io     = $io;
    }

    public function deactivate(Composer $composer, IOInterface $io)
    {
        // TODO: Implement deactivate() method.
    }

    public function uninstall(Composer $composer, IOInterface $io)
    {
        // TODO: Implement uninstall() method.
    }

    /**
     * @param PreFileDownloadEvent $event
     * @return void
     * @throws Exception
     */
    public function onPreFileDownload(PreFileDownloadEvent $event)
    {
        $uri = new RequestUri($event->getProcessedUrl());
        $this->debug("Check URL: {$event->getProcessedUrl()}");
        if ($uri->getScheme() === 's3') {
            $url = $this->getSinger()->createPresignedLink($uri, $this->getConfig($uri));
            $event->setProcessedUrl($url);
            $this->debug("replace URL {$url}");
        }
    }

    private function getSinger()
    {
        return $this->singer ?: ($this->singer = new SignatureV4());
    }

    private function debug($message)
    {
        if ($this->io->isDebug()) {
            $this->io->write("S3: {$message}");
        }
    }

    /**
     * @param RequestUri $uri
     * @return mixed
     */
    public function getConfig(RequestUri $uri)
    {
        $config = $this->config[$uri->getHost()] ?? null;

        if (!$config) {
            throw new RuntimeException("Config for \"{$uri->getHost()}\" not found");
        }
        return $config;
    }

}
