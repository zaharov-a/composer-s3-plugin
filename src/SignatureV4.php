<?php

namespace Zakharov\ComposerS3;

use DateTimeImmutable;
use DateTimeZone;
use Exception;

/**
 * обрезанный класс \Aws\Signature\SignatureV4 из aws/aws-sdk-php
 */
class SignatureV4
{
    const DATE_ISO8601_BASIC = 'Ymd\THis\Z';
    const DATE_SHORT = 'Ymd';

    const AMZ_ALGORITHM = 'AWS4-HMAC-SHA256';

    const UNSIGNED_PAYLOAD = 'UNSIGNED-PAYLOAD';

    private $service = 's3';

    private $cacheSize = 0;

    /**
     * @var array
     */
    private $cache = [];

    /**
     * @param RequestUri $uri
     * @param array $config
     * @param DateTimeImmutable|null $date
     * @return string
     * @throws Exception
     * @see https://docs.aws.amazon.com/AmazonS3/latest/API/sigv4-query-string-auth.html
     */
    public function createPresignedLink(RequestUri $uri, $config, DateTimeImmutable $date = null)
    {
        if (is_null($date)) {
            $date = new DateTimeImmutable('now', new DateTimeZone("UTC"));
        }
        $dateTo = $date->modify($config['expires'] ?? '+2 minutes');

        $scope = $this->createScope($date, $config['region']);

        $query = [
            'X-Amz-Algorithm'     => self::AMZ_ALGORITHM,
            'X-Amz-Credential'    => "{$config['access-key']}/{$scope}",
            'X-Amz-Date'          => $date->format(self::DATE_ISO8601_BASIC),
            'X-Amz-Expires'       => $dateTo->getTimestamp() - $date->getTimestamp(),
            'X-Amz-SignedHeaders' => 'host',
        ];

        return str_replace('s3://', 'https://', $uri->getFull()) . '?'
            . http_build_query($this->sing($query, $config, $uri, $date));
    }

    private function createContext(RequestUri $uri, $query, $headers)
    {
        $parsedRequest = new RequestUri($uri->getFull() . '?' . http_build_query($query));

        // Normalize the path as required by SigV4
        $canon = "GET\n"
            . $this->createCanonicalizedPath($parsedRequest->getPath()) . "\n"
            . $parsedRequest->getQuery() . "\n";

        // Case-insensitively aggregate all of the headers.
        $aggregate = [];
        foreach ($headers as $key => $values) {
            $key             = strtolower($key);
            $aggregate[$key] = [$values];
        }

        ksort($aggregate);
        $canonHeaders = [];
        foreach ($aggregate as $k => $v) {
            if (count($v) > 0) {
                sort($v);
            }
            $canonHeaders[] = $k . ':' . preg_replace('/\s+/', ' ', implode(',', $v));
        }

        $signedHeadersString = implode(';', array_keys($aggregate));
        $canon               .= implode("\n", $canonHeaders) . "\n\n"
            . $signedHeadersString . "\n"
            . self::UNSIGNED_PAYLOAD;

        return ['creq' => $canon, 'headers' => $signedHeadersString];
    }

    protected function createCanonicalizedPath($path)
    {
        $doubleEncoded = rawurlencode(ltrim($path, '/'));

        return '/' . str_replace('%2F', '/', $doubleEncoded);
    }

    private function getSigningKey($shortDate, $region, $secretKey)
    {
        $k = $shortDate . '_' . $region . '_' . $this->service . '_' . $secretKey;

        if (!isset($this->cache[$k])) {
            // Clear the cache when it reaches 50 entries
            if (++$this->cacheSize > 50) {
                $this->cache     = [];
                $this->cacheSize = 0;
            }

            $dateKey         = hash_hmac(
                'sha256',
                $shortDate,
                "AWS4{$secretKey}",
                true
            );
            $regionKey       = hash_hmac('sha256', $region, $dateKey, true);
            $serviceKey      = hash_hmac('sha256', $this->service, $regionKey, true);
            $this->cache[$k] = hash_hmac(
                'sha256',
                'aws4_request',
                $serviceKey,
                true
            );
        }
        return $this->cache[$k];
    }

    /**
     * @param string $ldt
     * @param string $scope
     * @param string $creq
     * @return string
     */
    public function createStringToSign($ldt, $scope, $creq)
    {
        return self::AMZ_ALGORITHM . "\n" .
            $ldt . "\n" .
            $scope . "\n" .
            hash('sha256', $creq);
    }

    /**
     * @param array $headers
     * @param array $config
     * @param RequestUri $uri
     * @param DateTimeImmutable $date
     * @return array
     */
    public function sing($headers, $config, RequestUri $uri, DateTimeImmutable $date)
    {
        $context = $this->createContext($uri, $headers, ['host' => $uri->getHost()]);

        $stringToSign = $this->createStringToSign(
            $date->format(self::DATE_ISO8601_BASIC),
            $this->createScope($date, $config['region']),
            $context['creq']
        );

        $signingKey = $this->getSigningKey(
            $date->format(self::DATE_SHORT),
            $config['region'],
            $config['secret-key']
        );

        $headers['X-Amz-Signature'] = hash_hmac('sha256', $stringToSign, $signingKey);
        return $headers;
    }

    /**
     * @param DateTimeImmutable $date
     * @param string $region
     * @return string
     */
    public function createScope(DateTimeImmutable $date, $region)
    {
        $sdt = $date->format(self::DATE_SHORT);
        return "{$sdt}/{$region}/{$this->service}/aws4_request";
    }
}
