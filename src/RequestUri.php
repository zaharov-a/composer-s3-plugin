<?php

namespace Zakharov\ComposerS3;

class RequestUri
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var array
     */
    private $parts;

    /**
     * @param string $url
     */
    public function __construct($url)
    {
        $this->url   = $url;
        $this->parts = parse_url($url);
    }

    public function getFull()
    {
        return $this->url;
    }

    public function getScheme()
    {
        return $this->parts['scheme'];
    }

    public function getHost()
    {
        return $this->parts['host'];
    }

    public function getQuery()
    {
        return $this->parts['query'] ?? '';
    }

    public function getPath()
    {
        return $this->parts['path'] ?? '';
    }

}
