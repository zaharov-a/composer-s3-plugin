<?php

namespace Zakharov\ComposerS3;

use Exception;
use PHPUnit\Framework\TestCase;

class SignatureV4Test extends TestCase
{
    /**
     * @return void
     * @throws Exception
     * @see https://docs.aws.amazon.com/AmazonS3/latest/API/sigv4-query-string-auth.html#query-string-auth-v4-signing-example
     */
    public function testCreatePresignedLink()
    {
        $uri = new RequestUri('https://examplebucket.s3.amazonaws.com/test.txt');
        $config = [
            'access-key' => 'AKIAIOSFODNN7EXAMPLE',
            'secret-key' => 'wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY',
            'region'     => 'us-east-1',
            'expires'    => '+24 hours',
        ];
        $singer = new SignatureV4();

        $expectedUrl = 'https://examplebucket.s3.amazonaws.com/test.txt?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIOSFODNN7EXAMPLE%2F20130524%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20130524T000000Z&X-Amz-Expires=86400&X-Amz-SignedHeaders=host&X-Amz-Signature=aeeed9bbccd4d02ee5c0109b86d86835f995330da4c265957d157751f604d404';
        parse_str(parse_url($expectedUrl, PHP_URL_QUERY), $expected);
        parse_str(
            parse_url(
                $singer->createPresignedLink($uri, $config, new \DateTimeImmutable('20130524T000000Z')),
                PHP_URL_QUERY
            ),
            $actual
        );

        $this->assertEquals(
            $expected,
            $actual
        );
    }
}
