# composer-s3-plugin
Плагин для работы с приватными репозиториями размещенных на s3-хранилищах
> Создание приватного репозитория https://getcomposer.org/doc/articles/handling-private-packages.md
## Установка:  
```shell
composer global req zakharov/composer-s3-plugin
```
Настройка:
```json
{
    "repositories": [
        {
            "type": "composer",
            "url": "s3://<BUCKET_HOST>/[REPOSITORY_PATH/]"
        }
    ],
    "config": {
        "s3": {
            "<BUCKET_HOST>": {
                "access-key": "<ACCESS_KEY>",
                "secret-key": "<SECRET_KEY>",
                "region": "<REGION>",
                "expires": "[strtotime compatible, default = +2 minutes]"
            }
        }
    }
}
```

## Пример:  
```json
{
    "repositories": [
        {
            "type": "composer",
            "url": "s3://bucket.storage.yandexcloud.net/"
        }
    ],
    "config": {
        "s3": {
            "bucket.storage.yandexcloud.net": {
                "access-key": "ASDwer234ASdf",
                "secret-key": "ASdq2r312asdASd",
                "region": "ru-central1"
            }
        }
    }
}
```
